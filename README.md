## fleur-user 11 RP1A.200720.011 V13.0.2.0.RKETWXM release-keys
- Manufacturer: xiaomi
- Platform: mt6781
- Codename: fleur
- Brand: Redmi
- Flavor: fleur-user
fleur-user
fleur-user
miel-user
miel-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: V13.0.2.0.RKETWXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/fleur/fleur:11/RP1A.200720.011/V13.0.2.0.RKETWXM:user/release-keys
- OTA version: 
- Branch: fleur-user-11-RP1A.200720.011-V13.0.2.0.RKETWXM-release-keys
- Repo: redmi_fleur_dump_4952


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
